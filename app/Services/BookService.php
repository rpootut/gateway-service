<?php 

namespace App\Services;
use App\Traits\ConsumesExternalService;


class BookService{
    use ConsumesExternalService;

    public function __construct(){
        $this->baseUri = config('services.books.base_uri');
    }
}