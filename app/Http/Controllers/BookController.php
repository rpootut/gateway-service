<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    //
    public function index(){

    }

    public function store(Request $request){

    }

    public function update(Request $request, $book){

    }

    public function show($book){

    }

    public function destroy($book){

    }
}
