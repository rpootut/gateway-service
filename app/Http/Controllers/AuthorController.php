<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Services\AuthorService;

class AuthorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public $authorService;

    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    //
    public function index(){
        $authors = $this->authorService->obtainAuthors();
        return $authors;
    }

    public function store(Request $request){
        $author = $this->authorService->createAuthor($request->all());
        return $author; 
    }

    public function update(Request $request, $author){
        $author = $this->authorService->editAuthor($request->all(), $author);
        return $author; 
    }

    public function show($author){
        $author = $this->authorService->obtainAuthor($author);
        return $author;
    }

    public function destroy($author){
        //tiernito :3
        $author = $this->authorService->deleteAuthor($author);
        return $author;
    }
}
